'''
Name:IPL01.py
Desciption: Plot the number of matches played per year of all the years in IPL.
Input: Match data in dict format
Output: A plot of number of matches played in all years of IPL
Usage: * Install required dependencies using pip -r requirements.txt
       * python [filename]
author:abhiyan timilsina
'''
from helpers import get_ipl_data,plot_figure

def find_no_matches_all_year():
    match_data = get_ipl_data()['match_data']
    matches_by_year = {}

    for match in match_data:
        if match['season'] not in matches_by_year:
            matches_by_year[match['season']]=0
        matches_by_year[match['season']]+=1
    print(matches_by_year)
    matches_by_year = sorted(matches_by_year.items(),key=lambda kv: kv[0])
    plot_figure([i for i in range(1,len(matches_by_year)+1)],[match[1] for match in matches_by_year],
                 [match[0] for match in matches_by_year],['Years','Matches Played'],'No of Matches Played in all years of IPL')

find_no_matches_all_year()