import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database = 'IPLDB'
)
cursor = mydb.cursor()
sql_commands = ['SELECT COUNT(*),winner,season FROM MATCHES where result != \'no result\'  GROUP BY winner,season ORDER BY season',
'SELECT SUM(d.total_runs),d.bowler,COUNT(DISTINCT d.over) from MATCHES AS m , DELIVERIES AS d WHERE m.id = d.match_id and m.season=2015 GROUP BY d.bowler',
'SELECT COUNT(venue),venue from MATCHES GROUP BY venue', 
'SELECT COUNT(*) AS cnt,season FROM MATCHES GROUP BY season ORDER BY season',
'SELECT SUM(d.extra_runs),d.bowling_team from MATCHES as m,DELIVERIES AS d where m.id=d.match_id AND m.season=2016 GROUP BY d.bowling_team']

for sql_command in sql_commands:
    cursor.execute(sql_command)
    for row in cursor:
        print(row)
    print('-------------------------------------------------------------------------------------')
