'''
Name:IPL02.py
Desciption: Plot a stacked bar chart of matches won of all teams over all the years of IPL.
Input: Match data in dict format
Output: A plot of matches won by all the teams in all years of IPL
Usage: * Install required dependencies using pip -r requirements.txt
       * python [filename]
author:abhiyan timilsina
'''

from helpers import get_ipl_data
import matplotlib.pyplot as plt 
from collections import OrderedDict
import random

def find_no_matches_all_year():
    match_data = get_ipl_data()['match_data']
    matches_won_by_teams = {}

    for match in match_data:
        if match['season'] not in matches_won_by_teams:
            matches_won_by_teams[match['season']]={}
        if match['winner'] not in matches_won_by_teams[match['season']]:
            matches_won_by_teams[match['season']][match['winner']]=0
        matches_won_by_teams[match['season']][match['winner']]+=1
    
    matches_won_by_teams = sorted(matches_won_by_teams.items(),key=lambda kv: kv[0])
    get_colors = lambda n: list(map(lambda i: "#" + "%06x" % random.randint(0, 0xFFFFFF),range(n)))
    color_set = get_colors(1000)
    # color_set = [
    #     '#4E79A7', '#F28E2B', '#E15759', '#76B7B2',
    #     '#59A14F', '#EDC948', '#B07AA1', '#FF9DA7',
    #     '#BAB0AC', '#9C755F','#9A8C6A', '#55C6B0', '#2B5C16', '#F1D72E', '#72BED8', '#BF2454', '#91FBDA', '#C2CB6A', '#CBB0DD', '#E16178', '#EE18D5']

    year = 0
    team_wins=1
    x_pos = 1
    x_ticks = []
    team_color = {}
    for team_wins_year in matches_won_by_teams:
        bottom=0
        x_ticks.append(team_wins_year[year])
        for team_win in team_wins_year[team_wins].items():
            win_count = 1
            team_name = 0
            if team_win[team_name] not in team_color:
                color_pos = random.randint(0,len(color_set)-1)
                team_color[team_win[team_name]]=color_set[color_pos]
                del color_set[color_pos]
                
            plt.bar(x_pos,team_win[win_count],bottom=bottom,label=team_win[team_name],color=team_color[team_win[team_name]])
            bottom+=team_win[win_count]
        x_pos+=1
    
    plt.xticks([x for x in range(1,len(matches_won_by_teams)+1)],x_ticks,rotation='vertical')
    plt.xlabel('Years')
    plt.ylabel('Team Win Count')
    plt.tight_layout()
    plt.legend()
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys())
    
    plt.show()

     
find_no_matches_all_year()
        

