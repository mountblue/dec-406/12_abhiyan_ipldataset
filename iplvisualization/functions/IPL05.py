'''
Name:IPL05.py
Desciption: Plot the number of matches played per stadium.
Input: Match data in dict format
Output: A plot of number of matches played in each stadium
Usage: * Install required dependencies using pip -r requirements.txt
       * python [filename]
author:abhiyan timilsina
'''

from helpers import get_ipl_data,plot_figure


def stadiums_ipl_count():
    ipl_data = get_ipl_data()
    match_data = ipl_data['match_data']
    stadium_match_count={}

    
    for match in match_data:
        if match['venue'] not in stadium_match_count:
            stadium_match_count[match['venue']]=0
        stadium_match_count[match['venue']]+=1
    plot_figure([x for x in range(1,len(stadium_match_count.items())+1)],[tuple[1] for tuple in stadium_match_count.items()],
    [tuple[0] for tuple in stadium_match_count.items()],['Stadium Names','Matches Played'],'Stadiums and Matches played IPL')

stadiums_ipl_count()