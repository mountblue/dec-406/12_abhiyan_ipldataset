'''
Name:IPL03.py
Desciption: For the year 2016 plot the extra runs conceded per team.
Input: Match data in dict format
Output: A plot of extra run conceded per team
Usage: * Install required dependencies using pip -r requirements.txt
       * python [filename]
author:abhiyan timilsina
'''

from helpers import get_ipl_data,plot_figure,filter_matches_by_year

def find_extra_runs_2016():
    match_ids_2016 = filter_matches_by_year(2016)
    deliveries_data = get_ipl_data()['deliveries_data']
    team_extra_runs = {}

    for delivery in deliveries_data:
        if delivery['match_id'] not in match_ids_2016:
            continue
        if delivery['bowling_team'] not in team_extra_runs:
            team_extra_runs[delivery['bowling_team']] = 0
        team_extra_runs[delivery['bowling_team']] +=int(delivery['extra_runs'])
    
    print(team_extra_runs)
    team_extra_runs = sorted(team_extra_runs.items(),key= lambda kv: kv[1])
    plot_figure([x for x in range(1,len(team_extra_runs)+1)],
    [row[1] for row in team_extra_runs],[row[0] for row in team_extra_runs],['Team Name','Extra Runs'],'Extra Runs 2016')
        
    
    
find_extra_runs_2016()

