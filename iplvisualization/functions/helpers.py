'''
Name:helpes.py
Desciption:This file contains functions needed for working with csv files
author:abhiyan timilsina
'''

import csv
import matplotlib.pyplot as plt

#Plot the graph 
def plot_figure(x_coordinates,y_coordinates,x_labels,axes_labels,title,type='bar'):
    plt.bar(x_coordinates,y_coordinates,width=0.5)
    plt.xlabel(axes_labels[0])
    plt.ylabel(axes_labels[1])
    plt.title(title)
    plt.xticks(x_coordinates,x_labels,rotation='vertical')
    plt.tight_layout()
    plt.show()

#Function to get the IPL data from csv file 
def get_ipl_data():
    matches_file_path = '../csv_files/matches.csv'
    deliveries_file_path = '../csv_files/deliveries.csv'
    match_data=[]
    deliveries_data = []

    with open(matches_file_path) as csvfile:
        raw_match_data = csv.DictReader(csvfile)
        for match in raw_match_data:
            match_data.append(match)

    with open(deliveries_file_path) as csvfile:
        raw_deliveries_data = csv.DictReader(csvfile)
        for entry in raw_deliveries_data:
            deliveries_data.append(entry)

    return {'match_data':match_data,'deliveries_data':deliveries_data}

#Function to filter match_id with specific year
def filter_matches_by_year(year,match_data=get_ipl_data()['match_data']):
    matches_in_year = []

    for match in match_data:
        if int(match['season']) == year:
            matches_in_year.append(match['id'])
             
    return matches_in_year

