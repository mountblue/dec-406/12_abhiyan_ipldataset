'''
Name:IPL04.py
Desciption: For the year 2015 plot the top economical bowlers.
Input: Match data in dict format
Output: A plot of most economical bowlers 2015.
Usage: * Install required dependencies using pip -r requirements.txt
       * python [filename]
author:abhiyan timilsina
'''

from helpers import get_ipl_data,plot_figure,filter_matches_by_year

def find_ecomomy_baller_2015():
    ipl_data = get_ipl_data()
    match_ids_2015 = filter_matches_by_year(2015,ipl_data['match_data'])
    deliveries_data = ipl_data['deliveries_data']
    bowler_economy ={}
    bowler_name = 0
    total_runs_bowler = 2
    total_overs = 1
    for row in deliveries_data:
        if row['match_id'] not in match_ids_2015:
            continue
        if row['bowler'] not in bowler_economy:
            bowler_economy[row['bowler']]=[row['over'],1,0]
        bowler_economy[row['bowler']][total_runs_bowler]+=int(row['total_runs'])
        if int(row['over']) != int(bowler_economy[row['bowler']][0]):
            bowler_economy[row['bowler']][total_overs]+=1
            bowler_economy[row['bowler']][0]=row['over']
    
    bowler_economy = bowler_economy.items()
    for bowler in bowler_economy:
        bowler_stats = 1
        economy = 0
        bowler[bowler_stats][economy]=bowler[bowler_stats][total_runs_bowler]/bowler[bowler_stats][total_overs]
    bowler_economy = sorted(bowler_economy,key=lambda kv: kv[bowler_stats][economy])[:5]
    plot_figure([x for x in range(1,6)],[bowler[bowler_stats][economy] for bowler in bowler_economy],
    [bowler[bowler_name] for bowler in bowler_economy],['Bowler Name','Economy'],'Top 5 economy player 2015')

find_ecomomy_baller_2015()
        
        
        
